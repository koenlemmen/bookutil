import net.kyori.adventure.inventory.Book;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.minimessage.MiniMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * The BookUtil class contains functions to check if a book is valid and to create a book from a string.
 */
public class BookUtil {

    private static final MiniMessage mm = MiniMessage.miniMessage();

    /**
     * The createPages function takes a string of text and returns a list of pages.
     * The function will split the input into lines, then check if each line is too long to fit on one page.
     * If it is, an exception will be thrown with an error message because if a line too long to fit on a single page
     * a book cannot be created.
     * Otherwise, the function will add each line to a page until that page becomes full (or there are no more lines).
     * Should prevent the creation of books with pages that are too long to fit on a single page.
     *
     * @param miniMessageWithEndOfLines A MiniMessage string with \n characters at the end of each line
     *
     * @return A list of components
     *
     * @throws UnsupportedOperationException If a line is too long to fit on a page, meaning a book could never be created
     */
    public static List<Component> createPages(String miniMessageWithEndOfLines) throws UnsupportedOperationException {
        List<Component> pages = new ArrayList<>();

        if (checkBook(miniMessageWithEndOfLines).isSuccess()) {
            pages.add(mm.deserialize(miniMessageWithEndOfLines));
            return pages;
        }

        String[] lines = miniMessageWithEndOfLines.split("\n");

        Component page = Component.empty();
        for (String line : lines) {
            CheckResult characherCheck = characterCheck(line);
            if (!characherCheck.isSuccess()) {
                throw new UnsupportedOperationException("Line " + line + " is too long to fit on a page! " + characherCheck.getMessage());
            }
            if (!checkBook(page.append(mm.deserialize(line))).isSuccess()) {
                pages.add(page);
                page = Component.empty();
            }
            page = page.append(mm.deserialize(line));
        }

        if (!page.equals(Component.empty())) {
            pages.add(page);
        }
        return pages;
    }

    /**
     * Overload of checkBook(String page)
     *
     * @param book A book object
     *
     * @return CheckResult Returns a CheckResult object with a boolean success value and a String message
     */
    public static CheckResult checkBook(Book book) {
        return checkBook(book.pages());
    }

    /**
     * Overload of checkBook(String page)
     *
     * @param pages A list of components
     *
     * @return CheckResult Returns a CheckResult object with a boolean success value and a String message
     */
    public static CheckResult checkBook(List<Component> pages) {
        for (Component page : pages) {
            CheckResult result = checkBook(page);
            if (!result.isSuccess()) {
                return result;
            }
        }
        return CheckResult.success();
    }

    /**
     * Overload of checkBook(String page)
     *
     * @param page A component
     *
     * @return CheckResult Returns a CheckResult object with a boolean success value and a String message
     */
    public static CheckResult checkBook(Component page) {
        return checkBook(mm.serialize(page));
    }

    /**
     * The checkBook function checks the page for character limit and line limit.
     *
     * @param page A string of text
     *
     * @return A checkresult object
     */
    public static CheckResult checkBook(String page) {
        CheckResult characterCheck = characterCheck(page);
        if (!characterCheck.isSuccess()) {
            return characterCheck;
        }
        CheckResult linesCheck = linesCheck(page);
        if (!linesCheck.isSuccess()) {
            return linesCheck;
        }
        return CheckResult.success();
    }

    /**
     * Overload of linesCheck(String page)
     *
     * @param book A book object
     *
     * @return CheckResult Returns a CheckResult object with a boolean success value and a String message
     */
    public static CheckResult characterCheck(Book book) {
        return characterCheck(book.pages());
    }

    /**
     * Overload of characterCheck(String page)
     *
     * @param pages A list of components
     *
     * @return CheckResult Returns a CheckResult object with a boolean success value and a String message
     */
    public static CheckResult characterCheck(List<Component> pages) {
        for (Component page : pages) {
            CheckResult result = characterCheck(page);
            if (!result.isSuccess()) {
                return result;
            }
        }
        return CheckResult.success();
    }

    /**
     * Overload of characterCheck(String page)
     *
     * @param page A component
     *
     * @return CheckResult Returns a CheckResult object with a boolean success value and a String message
     */
    public static CheckResult characterCheck(Component page) {
        return characterCheck(mm.serialize(page));
    }

    /**
     * The characterCheck function checks to see if the number of characters in a page is greater than 14 lines of 114
     * dots each. Minecraft uses characters that are 5 dots or bold 6 dots wide. The function strips all MiniMessage
     * tags from the page as these are not rendered client side.
     *
     * @param page A string of text or MiniMessage as string
     *
     * @return A checkresult
     */
    public static CheckResult characterCheck(String page) {
        String strippedPage = page.replaceAll("<.*?>", "");
        // 14 lines of 114 dots each, 6 dots per character
        if ((strippedPage.length() * 6) > 14 * 114) {
            return CheckResult.failure("Too many characters: " + strippedPage.length() + " text: " + page);
        }
        return CheckResult.success();
    }

    /**
     * Overload of linesCheck(String page)
     *
     * @param book A book object
     *
     * @return CheckResult Returns a CheckResult object with a boolean success value and a String message
     */
    public static CheckResult linesCheck(Book book) {
        return linesCheck(book.pages());
    }

    /**
     * Overload of linesCheck(String page)
     *
     * @param pages A list of components
     *
     * @return CheckResult Returns a CheckResult object with a boolean success value and a String message
     */
    public static CheckResult linesCheck(List<Component> pages) {
        for (Component page : pages) {
            CheckResult result = linesCheck(page);
            if (!result.isSuccess()) {
                return result;
            }
        }
        return CheckResult.success();
    }

    /**
     * Overload of linesCheck(String page)
     *
     * @param page A component
     *
     * @return CheckResult Returns a CheckResult object with a boolean success value and a String message
     */
    public static CheckResult linesCheck(Component page) {
        return linesCheck(mm.serialize(page));
    }

    /**
     * The linesCheck function takes a String as an argument and returns a CheckResult.
     * The function splits the string into lines, then checks if there are more than 14 lines.
     * If so, it returns a failure result with the number of lines in the text and the text itself.
     * Otherwise, it counts how many extra lines are created by truncation (by calling extraLinesFromLine)
     * and adds that to its count of total lines. If this new count is greater than 14, it returns failure result;
     * otherwise it returns success!
     *
     * @param page A string of text or MiniMessage as string
     *
     * @return CheckResult Returns a CheckResult object with a boolean success value and a String message
     */
    public static CheckResult linesCheck(String page) {
        String[] lines = page.split("\n");
        if (lines.length > 14) {
            return CheckResult.failure("Too many lines: " + lines.length + " text: " + page);
        }
        int lineCount = 0;
        for (String line : lines) {
            String strippedLine = line.replaceAll("<.*?>", "");
            if (!lengthOfLineCheck(strippedLine).isSuccess()) {
                lineCount += extraLinesFromLine(strippedLine);
            }
            lineCount++;
        }
        if (lineCount > 14) {
            return CheckResult.failure("Too many lines: " + lineCount + " text: " + page);
        }
        return CheckResult.success();
    }

    /**
     * The lengthOfLineCheck function checks if the length of a line is too long to be a single line of 114 dots.
     *
     * @param line A string of text, should be stripped of MiniMessage tags
     *
     * @return CheckResult Returns a CheckResult object with a boolean success value and a String message
     */
    public static CheckResult lengthOfLineCheck(String line) {
        if (line.length() * 6 > 114) {
            return CheckResult.failure("Too many characters in line: " + line.length() + " text: " + line);
        }
        return CheckResult.success();
    }

    /**
     * The extraLinesFromLine function takes a String as an argument and returns the number of extra lines that will be
     * needed to print it. It does this by multiplying the length of the line by 6 (the number of dots per character)
     * and dividing by 114 (the number of dots per line). It then rounds up to the nearest integer.
     *
     * @param line A string of text, should be stripped of MiniMessage tags
     *
     * @return The number of extra lines that are needed to print the line
     */
    public static int extraLinesFromLine(String line) {
        return (line.length() * 6) / 114;
    }

}

