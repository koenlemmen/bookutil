/**
 * The CheckResult class is used to determine whether a given check has passed.
 */
public class CheckResult {

    private final boolean success;
    private final String message;

    /**
     * The CheckResult function is used to determine whether a given
     * check has passed. It returns a boolean value of true if the check passes,
     * and false if it does not. The CheckResult function also contains an error message
     * that will be displayed in the event that the check fails. This allows for more detailed
     * information about why a particular test failed to be provided to users of this program.
     *
     * @param success Determine whether the result of a check is true or false
     * @param message Return a message to the user
     */
    public CheckResult(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    /**
     * The success function returns a CheckResult object with the boolean value true and an empty string.
     *
     * @return A CheckResult object with a boolean value of true and an empty string
     */
    public static CheckResult success() {
        return new CheckResult(true, "");
    }

    /**
     * The failure function returns a CheckResult object with the boolean value false and the message passed in as an argument.
     *
     * @param message Return a message to the user
     *
     * @return A CheckResult object with the boolean value false and a message
     */
    public static CheckResult failure(String message) {
        return new CheckResult(false, message);
    }

    /**
     * The isSuccess function returns a boolean value that indicates whether the result was successful.
     *
     * @return A boolean value
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * The getMessage function returns the message of the reason for failure.
     *
     * @return The message variable
     */
    public String getMessage() {
        return message;
    }
}
